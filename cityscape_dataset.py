import os
import torch
import numpy as np
from PIL import Image
from glob import glob

class CityscapeDataset(torch.utils.data.Dataset):
    def __init__(self, root, mode="train", transform=None):

        assert mode in {"train", "valid", "test"}

        self.root = root
        self.mode = mode
        self.transform = transform

        self.images_directory = os.path.join(self.root, "leftImg8bit", self.mode, "*/*png")
        self.masks_directory = os.path.join(self.root, "gtFine", self.mode)

        self.filenames = glob(self.images_directory)
        self.mask_paths = []
        for imagename in self.filenames:
            parent_dir = imagename.split("/")[-2]
            maskname = "_".join(imagename.split("/")[-1].split("_")[:3].extend(["gtFine", "labelTrainIds.png"]))
            mask_path = os.path.join(self.masks_directory, parent_dir, maskname)
            assert os.path.exists(mask_path), f"the label file {mask_path} could not found!"
            self.mask_paths.append(mask_path)

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, idx):
        image_path = self.filenames[idx]
        mask_path = self.mask_paths[idx]

        image = np.array(Image.open(image_path).convert("RGB"))

        trimap = np.array(Image.open(mask_path))
        mask = self._preprocess_mask(trimap)

        sample = dict(image=image, mask=mask, trimap=trimap)
        if self.transform is not None:
            sample = self.transform(**sample)

        return sample

    @staticmethod
    def _preprocess_mask(mask):
        mask = mask.astype(np.float32)
        mask[mask == 2.0] = 0.0
        mask[(mask == 1.0) | (mask == 3.0)] = 1.0
        return mask

