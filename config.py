#!usr/bin/env python  
# -*- coding:utf-8 -*-  
""" 
*******************************************************************************************
              ,---._
            .-- -.' \                                   ,--,     ,--,
            |    |   :  ,--,                            |'. \   / .`|
            :    ;   |,--.'|         ,---,              ; \ `\ /' / ;          ,--,
            :        ||  |,      ,-+-. /  |  ,----._,.  `. \  /  / .'        ,'_ /|
            |    :   :`--'_     ,--.'|'   | /   /  ' /   \  \/  / ./    .--. |  | :
            :         ,' ,'|   |   |  ,"' ||   :     |    \  \.'  /   ,'_ /| :  . |
            |    ;   |'  | |   |   | /  | ||   | .\  .     \  ;  ;    |  ' | |  . .
        ___ l         |  | :   |   | |  | |.   ; ';  |    / \  \  \   |  | ' |  | |
      /    /\    J   :'  : |__ |   | |  |/ '   .   . |   ;  /\  \  \  :  | : ;  ; |
     /  ../  `..-    ,|  | '.'||   | |--'   `---`-'| | ./__;  \  ;  \ '  :  `--'   \
     \    \         ; ;  :    ;|   |/       .'__/\_: | |   : / \  \  ;:  ,      .-./
      \    \      ,'  |  ,   / '---'        |   :    : ;   |/   \  ' | `--`----'
       "---....--'     ---`-'                \   \  /  `---'     `--`
                                              `--`-'

                            Created at 2022/07/26 06:29
*******************************************************************************************
"""

import argparse
import os
import torch


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


class Parameters():
    def __init__(self):
        # set dataset
        parser = argparse.ArgumentParser(description="Pytorch Segmentation Network")
        parser.add_argument("--dataset", type=str, default="cityscapes",
                            help="Specify the dataset to use.")
        parser.add_argument("--batch-size", type=int, default=8,
                            help="Number of images sent to the network in one step.")
        parser.add_argument("--data-dir", type=str,
                            default='',
                            help="Path to the directory containing the PASCAL VOC dataset.")

        # network
        parser.add_argument("--input-size", type=int, default=769,
                            help="Comma-separated string with height and width of images.")
        parser.add_argument("--arch", type=str, default='unet',
                            help="segmentation model arch")
        parser.add_argument("--encoder-name", type=str, default='resnet34',
                            help="segmentation model backbone")
        parser.add_argument("--in-channels", type=int, default=3,
                            help="segmentation model input channels")
        parser.add_argument("--num-classes", type=int, default=19,
                            help="Number of classes to predict (including background).")
        parser.add_argument("--resume", type=str,
                            default=None,
                            help="Where restore model parameters from.")
        parser.add_argument("--sync-bn", action="store_true",
                            help="sync batchnorm")

        # training params
        parser.add_argument("--lr", type=float, default=0.00334,
                            help="Base learning rate for training with polynomial decay.")
        parser.add_argument("--momentum", type=float, default=0.9,
                            help="Momentum component of the optimiser.")
        parser.add_argument("--weight-decay", type=float, default=5e-4,
                            help="Regularisation parameter for L2-loss.")
        parser.add_argument("--seed", type=int, default=304,
                            help="Random seed to have reproducible results.")
        parser.add_argument("--log-dir", type=str, default='./log/',
                            help="Where to save snapshots of the model.")
        parser.add_argument("--epochs", type=int, default=120,
                            help="Number of the overall training epochs.")
        parser.add_argument('--local_rank', type=int, default=-1, help="set local rank")
        self.parser = parser

    def parse(self):
        args = self.parser.parse_args()
        return args
