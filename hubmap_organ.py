import cv2
import matplotlib.pyplot as plt
import numpy as np
import os
import random
from glob import glob
from torch.utils import data
import torch


class HubmapOrgan(data.Dataset):
    def __init__(self, root: str, mode: str, crop_size=(321, 321),
                 scale=True, mirror=True, rand_crop=True, trans=None):
        super().__init__()
        assert mode in ("train", "val"), f"value of param mode={mode} is invalid"

        self.root = root
        self.crop_h, self.crop_w = crop_size
        self.scale = scale
        self.is_mirror = mirror
        self.trans = trans
        listfile = os.path.join(root, f"{mode}.txt")

        self.files = []
        with open(listfile, 'r') as f:
            lines = [l for l in f.read().split("\n") if l != '']
        for line in lines:
            labelfile = line.replace(".tiff", ".png").replace("images", "labels")
            self.files.append({"img": line, "label": labelfile})

    def __len__(self):
        return len(self.files)

    def generate_scale_label(self, image, label):
        f_scale = 0.5 + random.randint(0, 16) / 10.0
        image = cv2.resize(image, None, fx=f_scale, fy=f_scale, interpolation=cv2.INTER_LINEAR)
        label = cv2.resize(label, None, fx=f_scale, fy=f_scale, interpolation=cv2.INTER_NEAREST)
        return image, label

    def random_crop(self, image, label):
        assert label.shape[-1] == 1, f"number of label channels is {label.shape[-1]}, not correct"

        np.random.seed(3456)
        cx, cy = 0.5 + np.random.uniform(-0.1, 0.1, 1), 0.5 + np.random.uniform(-0.1, 0.1, 1)
        h, w = image.shape[:-1]
        cx, cy = cx * w, cy * h
        width, height = np.random.uniform(0.3, 0.6, 1) * w, np.random.uniform(0.3, 0.6, 1) * h
        roi = list(map(int, [cx - 0.5 * width, cy - 0.5 * height, cx + 0.5 * width, cy + 0.5 * height]))
        crop_image = image[roi[1]:roi[3], roi[0]:roi[2], :]
        crop_label = label[roi[1]:roi[3], roi[0]:roi[2]]

        if not np.any(crop_label):
            crop_image, crop_label = self.random_crop(image, label)

        return crop_image, crop_label

    def __getitem__(self, index):
        datafiles = self.files[index]
        image = cv2.imread(datafiles["img"])
        label = cv2.imread(datafiles["label"], cv2.IMREAD_GRAYSCALE)

        # random crop image and label
        if np.random.uniform(0, 1, 1) > 0.5:
            image, label = self.random_crop(image, label)

        if self.scale:
            image, label = self.generate_scale_label(image, label)
        image = np.asarray(image, np.float32)

        img_h, img_w = label.shape
        pad_h = max(self.crop_h - img_h, 0)
        pad_w = max(self.crop_w - img_w, 0)
        if pad_h > 0 or pad_w > 0:
            img_pad = cv2.copyMakeBorder(image, 0, pad_h, 0,
                                         pad_w, cv2.BORDER_CONSTANT,
                                         value=(0.0, 0.0, 0.0))
            label_pad = cv2.copyMakeBorder(label, 0, pad_h, 0,
                                           pad_w, cv2.BORDER_CONSTANT,
                                           value=(0,))
        else:
            img_pad, label_pad = image, label

        img_h, img_w = label_pad.shape
        h_off = random.randint(0, img_h - self.crop_h)
        w_off = random.randint(0, img_w - self.crop_w)
        image = np.asarray(img_pad[h_off: h_off + self.crop_h, w_off: w_off + self.crop_w], np.float32)
        label = np.asarray(label_pad[h_off: h_off + self.crop_h, w_off: w_off + self.crop_w], np.float32)
        # image = image.transpose((2, 0, 1))
        if self.is_mirror:
            flip = np.random.choice(2) * 2 - 1
            image = image[:, :, ::flip]
            label = label[:, ::flip]

        image = np.ascontiguousarray(image)
        label = torch.from_numpy(np.ascontiguousarray(label))
        if self.trans is not None:
            image = self.trans(image)

        return image, label
