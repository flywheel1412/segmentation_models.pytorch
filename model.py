#!usr/bin/env python  
# -*- coding:utf-8 -*-  
""" 
*******************************************************************************************
              ,---._
            .-- -.' \                                   ,--,     ,--,
            |    |   :  ,--,                            |'. \   / .`|
            :    ;   |,--.'|         ,---,              ; \ `\ /' / ;          ,--,
            :        ||  |,      ,-+-. /  |  ,----._,.  `. \  /  / .'        ,'_ /|
            |    :   :`--'_     ,--.'|'   | /   /  ' /   \  \/  / ./    .--. |  | :
            :         ,' ,'|   |   |  ,"' ||   :     |    \  \.'  /   ,'_ /| :  . |
            |    ;   |'  | |   |   | /  | ||   | .\  .     \  ;  ;    |  ' | |  . .
        ___ l         |  | :   |   | |  | |.   ; ';  |    / \  \  \   |  | ' |  | |
      /    /\    J   :'  : |__ |   | |  |/ '   .   . |   ;  /\  \  \  :  | : ;  ; |
     /  ../  `..-    ,|  | '.'||   | |--'   `---`-'| | ./__;  \  ;  \ '  :  `--'   \
     \    \         ; ;  :    ;|   |/       .'__/\_: | |   : / \  \  ;:  ,      .-./
      \    \      ,'  |  ,   / '---'        |   :    : ;   |/   \  ' | `--`----'
       "---....--'     ---`-'                \   \  /  `---'     `--`
                                              `--`-'

                            Created at 2022/07/25 23:19
*******************************************************************************************
"""

import torch
import torch.nn as nn
import segmentation_models_pytorch as smp


class SegmentModel(nn.Module):
    def __init__(self, arch: str, encoder_name: str,
                 in_channels: int, out_classes: int):
        super(SegmentModel, self).__init__()
        # define model by smp
        self.model = smp.create_model(
            arch, encoder_name=encoder_name, in_channels=in_channels,
            classes=out_classes
        )

    def forward(self, x):
        return self.model(x)


if __name__ == '__main__':
    model = SegmentModel(arch='Unet', encoder_name='resnet34', in_channels=3, out_classes=5)

    dummay = torch.randn((1, 3, 640, 640), dtype=torch.float)

    output = model(dummay)

    print(output.shape)
