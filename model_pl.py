import pytorch_lightning as pl
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import segmentation_models_pytorch as smp
from pytorch_lightning.utilities.types import TRAIN_DATALOADERS, EVAL_DATALOADERS
from cityscape import *


class SegmentationModel(pl.LightningModule):
    def __init__(self, arch, encoder_name, in_channels, out_classes, opt, ignore_index=255, **kwargs):
        super().__init__()
        self.opt = opt
        # define model graph by smp module
        self.model = smp.create_model(
            arch, encoder_name=encoder_name, in_channels=in_channels, classes=out_classes, **kwargs
        )

        # preprocessing parameteres for image
        params = smp.encoders.get_preprocessing_params(encoder_name)
        self.register_buffer("std", torch.tensor(params["std"]).view(1, 3, 1, 1))
        self.register_buffer("mean", torch.tensor(params["mean"]).view(1, 3, 1, 1))

        # for image segmentation dice loss could be the best first choice
        self.loss_fn = smp.losses.DiceLoss(smp.losses.MULTICLASS_MODE,
                                           from_logits=True, ingore_index=ignore_index)

        # prepare dataset
        self.train_dataset = CitySegmentationTrain("../cityscapes", crop_size=(769, 769))
        self.val_dataset = CitySegmentationTest("../cityscapes", crop_size=(769, 769))

    def prepare_data(self) -> None:
        pass

    def forward(self, x):
        # in lightning, forward defines the prediction/inference actions
        x = (x - self.mean) / self.std
        mask = self(x)
        return mask

    def training_step(self, batch, batch_idx):
        # training_step defines the train loop. It is independent of forward
        x, gt = batch
        x = x.view(x.size(0), -1)
        pred = self(x)
        loss = self.loss_fn(pred, gt).unsqueeze(0)
        self.log("train_loss", loss, on_step=True)
        return {"train_loss": loss, "loss": loss}

    def validation_step(self, batch, batch_idx):
        x, gt = batch
        x = x.view(x.size(0), -1)
        pred = self(x)
        loss = self.loss_fn(pred, gt)
        self.log("train_loss", loss, on_step=True)

    def train_dataloader(self) -> TRAIN_DATALOADERS:
        return DataLoader(dataset=self.train_dataset, batch_size=4, shuffle=True, num_workers=8)

    def val_dataloader(self) -> EVAL_DATALOADERS:
        return [DataLoader(dataset=self.train_dataset, batch_size=4, shuffle=False, num_workers=8), ]

    def configure_optimizers(self):
        optimizer = torch.optim.SGD(self.model.parameters(), lr=self.opt.lr, weight_decay=3e-6)
        scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, max_lr=10 * self.opt.lr,
                                                        epochs=self.opt.epochs,
                                                        steps_per_epoch=len(self.train_dataloader()))
        return [optimizer, ], [scheduler, ]

    def validation_epoch_end(self, outputs):
        pass


