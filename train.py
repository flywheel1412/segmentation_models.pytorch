import torch
from model import SegmentModel
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.data import DataLoader, DistributedSampler
from cityscape import CitySegmentationTrain
import segmentation_models_pytorch as smp
from torchvision import transforms
from config import Parameters
from tqdm import tqdm
import torch.optim as optim
import torch.nn as nn
import random, os
from dice import DiceLoss, dice_coefficient
from utils import LogInfo, one_cycle


@torch.no_grad()
def validation(model: nn.Module, val_dataloader: DataLoader, device, opt, logger: LogInfo, epoch: int):
    model.eval()

    # inference
    mloss = torch.zeros(1, device=device)
    pbar = tqdm(enumerate(val_dataloader), total=len(val_dataloader), desc='Eval')
    preds, pred_logits, labels = [], [], []
    for i, (imgs, targets) in pbar:
        imgs = imgs.to(device)
        targets = targets.long().to(device)

        pred = model(imgs)

        # copy results to cpu memory
        mloss = (mloss * i + dice_coefficient(pred, targets, opt.num_classes)) / (i + 1)
        preds.append(pred.cpu())
        labels.append(targets.cpu())

    # statistic
    mloss = mloss.cpu().item()
    predicts = torch.cat(preds, dim=0)
    gts = torch.cat(labels, dim=0)

    predicts = predicts.softmax(dim=1)
    predicts = torch.argmax(predicts, dim=1)
    tp, fp, fn, tn = smp.metrics.get_stats(predicts, gts, mode='multiclass',
                                           ignore_index=255, num_classes=opt.num_classes)

    # metrics
    results = dict({})
    metrics = [smp.metrics.accuracy,
               smp.metrics.f1_score,
               smp.metrics.iou_score]
    logger.tb_add("val/dice", mloss, epoch)
    results['dice'] = mloss

    for metric in metrics:
        name = metric.__name__
        acc = metric(tp, fp, fn, tn, reduction='micro-imagewise')
        logger.tb_add(f"val/{name}", acc.item(), epoch)
        results[name] = acc.item()

    return results


def train(opt, LOCAL_RANK):
    # init
    device = torch.device('cuda', LOCAL_RANK)
    torch.cuda.set_device(device)
    dist.init_process_group(backend='nccl')
    torch.backends.cudnn.benchmark = True

    # Logger
    if LOCAL_RANK == 0:
        logger = LogInfo(opt.log_dir)
        logger.info("Input arguments:")
        for key, val in vars(opt).items():
            logger.info("{:16} {}".format(key, val))

    # model
    model = SegmentModel(opt.arch, opt.encoder_name, opt.in_channels, opt.num_classes)
    if opt.resume is not None:
        params = torch.load(opt.resume)
        if LOCAL_RANK == 0:
            logger.info(f"loading checkpoint from {opt.resume}")
        model.load_state_dict(params.state_dict(), strict=False)
    model = model.to(device)

    # dataset
    trans = transforms.Compose([transforms.ToTensor(),
                                transforms.Normalize(mean=[0.41738699, 0.45732192, 0.46886091],
                                                     std=[0.25685097, 0.26509955, 0.29067996])])
    if opt.dataset.lower() == "cityscapes":
        s = (opt.input_size, opt.input_size)

        train_dataset = CitySegmentationTrain(root=opt.data_dir, mode="train", crop_size=s, trans=trans)
        val_dataset = CitySegmentationTrain(root=opt.data_dir, mode="val", crop_size=s, trans=trans)
    else:
        if LOCAL_RANK == 0:
            logger.fatal(f"dataset type {opt.dataset.lower()} is not support")
            exit(-1)
    train_sampler = DistributedSampler(train_dataset, shuffle=True, seed=opt.seed)
    train_loader = DataLoader(train_dataset, batch_size=opt.batch_size,
                              sampler=train_sampler, num_workers=4, pin_memory=True)
    if LOCAL_RANK == 0:
        val_loader = DataLoader(val_dataset, batch_size=opt.batch_size,
                                num_workers=4, pin_memory=True)

    # optimizer
    lf = one_cycle(1, 0.17, opt.epochs)
    optimizer = optim.SGD(model.parameters(), lr=opt.lr, momentum=opt.momentum,
                          weight_decay=opt.weight_decay)
    scheduler = optim.lr_scheduler.LambdaLR(optimizer, lr_lambda=lf)
    scaler = torch.cuda.amp.GradScaler(enabled=True)

    # DDP
    if opt.sync_bn and LOCAL_RANK != -1:
        model = torch.nn.SyncBatchNorm.convert_sync_batchnorm(model).to(device)
        if LOCAL_RANK == 0:
            logger.info('Using SyncBatchNorm()')
    model = DDP(model, device_ids=[LOCAL_RANK], output_device=LOCAL_RANK)

    # loss
    loss_store = [nn.CrossEntropyLoss(ignore_index=255).to(device=device),
                  smp.losses.FocalLoss(smp.losses.MULTICLASS_MODE, ignore_index=255).to(device=device),
                  smp.losses.DiceLoss(smp.losses.MULTICLASS_MODE, ignore_index=255).to(device=device)]
    loss_store_names = [l._get_name() for l in loss_store]

    # training
    best_dice_loss = 1.5
    for epoch in range(opt.epochs):
        model.train()

        mloss = torch.zeros(len(loss_store), device=device)
        pbar = enumerate(train_loader)
        if LOCAL_RANK in {-1, 0}:
            pbar = tqdm(pbar, total=len(train_loader), desc=f"Train epoch {epoch}/{opt.epochs}")  # progress bar
        optimizer.zero_grad()
        for i, (imgs, targets) in pbar:
            imgs = imgs.to(device, non_blocking=True).float()
            targets = targets.long().to(device)

            # forward
            with torch.cuda.amp.autocast():
                pred = model(imgs)
                losses = [ll(pred, targets) * 4 for ll in loss_store]

                total_loss = losses[1]  # focal loss

            # backward
            scaler.scale(total_loss).backward()

            # optimize
            scaler.step(optimizer)  # optimizer.step
            scaler.update()
            optimizer.zero_grad()

            # Log
            if LOCAL_RANK == 0:
                for ii in range(len(loss_store)):
                    mloss[ii] = (mloss[ii] * i + losses[ii].detach()) / (i + 1)
                # pbar.set_description(('%10s' * 2 + '%10.4g' * len(loss_store)) %
                #                      (f'{epoch}/{opt.epochs - 1}', *mloss))

        # ------------ single epoch end ------------
        if LOCAL_RANK == 0:
            for ii in range(len(loss_store)):
                logger.tb_add(f"train/{loss_store_names[ii]}", mloss[ii].item(), epoch)

        # Scheduler
        lr = [x['lr'] for x in optimizer.param_groups]  # for loggers
        if LOCAL_RANK == 0:
            logger.tb_add(f"train/lr", lr[0], epoch)
        scheduler.step()

        # validation
        if LOCAL_RANK == 0:
            logger.info(f"evalidation for epoch {epoch}")
            val_metrics = validation(model, val_loader, device, opt, logger, epoch)

            # save best
            if val_metrics['dice'] < best_dice_loss:
                best_dice_loss = val_metrics['dice']
                save_name = f"epoch_{epoch}_diceLoss_{best_dice_loss:4f}.pth"
                torch.save(model.modules, os.path.join(opt.log_dir, save_name))
                logger.info(f"saveing best to {save_name}")


if __name__ == '__main__':
    opt = Parameters().parse()
    LOCAL_RANK = opt.local_rank

    # check params
    assert opt.data_dir != ''

    random.seed(opt.seed)
    torch.manual_seed(opt.seed)

    train(opt, LOCAL_RANK)
