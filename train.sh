#! /bin/bash

export OMP_NUM_THREADS=8
CUDA_VISIBLE_DEVICES=0,1,2,3 \
  python -m torch.distributed.launch \
  --nproc_per_node=4 train.py \
  --dataset cityscapes \
  --batch-size 8 \
  --data-dir ../cityscapes \
  --input-size 800 \
  --arch unet \
  --sync-bn \
  --encoder-name resnet34 \
  --in-channels 3 \
  --num-classes 19 \
  --epochs 100 \
  --log-dir run/7-29_unet_fix_dice
