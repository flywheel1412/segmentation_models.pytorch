import os
import torch
import matplotlib.pyplot as plt
import pytorch_lightning as pl
import argparse
from pytorch_lightning.utilities.cli import LightningCLI
from pytorch_lightning.strategies import DDPStrategy
from pprint import pprint
from model_pl import SegmentationModel
from cityscape import CityscapeDataModule


def train(opt):
    ddp = DDPStrategy(process_group_backend="nccl")
    trainer = pl.Trainer(accelerator="gpu", devices=4, strategy=ddp)

    cli = LightningCLI(CityscapeDataModule, CityscapeDataModule, seed_everything_default=1234, save_config_overwrite=True, run=False)
    cli.trainer.fit(cli.model, datamodule=cli.datamodule)
    cli.trainer.test(ckpt_path="best", datamodule=cli.datamodule)
    predictions = cli.trainer.predict(ckpt_path="best", datamodule=cli.datamodule)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Segmentation Training Script")
    parser.add_argument("--use_gpu", action="store_true", default=False, help="Whether to use GPU in the cloud")
    parser.add_argument('--local_rank', type=int, default=-1, help="set local rank")
    opt = parser.parse_args()
    
    
    