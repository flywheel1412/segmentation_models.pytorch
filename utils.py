#!usr/bin/env python  
# -*- coding:utf-8 -*-  
""" 
*******************************************************************************************
              ,---._
            .-- -.' \                                   ,--,     ,--,
            |    |   :  ,--,                            |'. \   / .`|
            :    ;   |,--.'|         ,---,              ; \ `\ /' / ;          ,--,
            :        ||  |,      ,-+-. /  |  ,----._,.  `. \  /  / .'        ,'_ /|
            |    :   :`--'_     ,--.'|'   | /   /  ' /   \  \/  / ./    .--. |  | :
            :         ,' ,'|   |   |  ,"' ||   :     |    \  \.'  /   ,'_ /| :  . |
            |    ;   |'  | |   |   | /  | ||   | .\  .     \  ;  ;    |  ' | |  . .
        ___ l         |  | :   |   | |  | |.   ; ';  |    / \  \  \   |  | ' |  | |
      /    /\    J   :'  : |__ |   | |  |/ '   .   . |   ;  /\  \  \  :  | : ;  ; |
     /  ../  `..-    ,|  | '.'||   | |--'   `---`-'| | ./__;  \  ;  \ '  :  `--'   \
     \    \         ; ;  :    ;|   |/       .'__/\_: | |   : / \  \  ;:  ,      .-./
      \    \      ,'  |  ,   / '---'        |   :    : ;   |/   \  ' | `--`----'
       "---....--'     ---`-'                \   \  /  `---'     `--`
                                              `--`-'

                            Created at 2022/07/26 07:12
*******************************************************************************************
"""

from torch.utils.tensorboard import SummaryWriter
import logging
import os, math


class LogInfo:
    def __init__(self, log_dir: str):
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        self.writer = SummaryWriter(log_dir=log_dir)

        logger = logging.getLogger("logger")
        logger.setLevel(logging.INFO)
        sh = logging.StreamHandler()
        log_file = os.path.join(log_dir, "train.log")
        fh = logging.FileHandler(log_file, encoding="UTF-8")

        formatter = logging.Formatter(fmt="%(asctime)s %(levelname)s %(message)s",
                                      datefmt="%Y/%m/%d %X")
        sh.setFormatter(formatter)
        fh.setFormatter(formatter)
        logger.addHandler(sh)
        logger.addHandler(fh)

        self.logger = logger

    def tb_add(self, tag: str, scalar, step: int):
        self.writer.add_scalar(tag, scalar, step)
        self.logger.info(f"{tag} {scalar} {step}")

    def info(self, msg: str):
        self.logger.info(msg)

    def error(self, msg: str):
        self.logger.error(msg)

    def warning(self, msg: str):
        self.logger.warning(msg)

    def fatal(self, msg: str):
        self.logger.fatal(msg)


def one_cycle(y1=0.0, y2=1.0, steps=100):
    # lambda function for sinusoidal ramp from y1 to y2 https://arxiv.org/pdf/1812.01187.pdf
    return lambda x: ((1 - math.cos(x * math.pi / steps)) / 2) * (y2 - y1) + y1
